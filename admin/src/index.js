/*! DDB-CMS recommender widget, copyright Veduz ApS */
import React from "react";
import ReactDOM from "react-dom";
import axios from "axios";

import { withStyles } from "@material-ui/styles";
import Grid from "@material-ui/core/Grid";
import Button from "@material-ui/core/Button";
import CssBaseline from "@material-ui/core/CssBaseline";
import Container from "@material-ui/core/Container";
import Drawer from "@material-ui/core/Drawer";
import Slider from "@material-ui/lab/Slider";
import FormGroup from "@material-ui/core/FormGroup";
import FormLabel from "@material-ui/core/FormLabel";
import RadioGroup from "@material-ui/core/RadioGroup";
import Radio from "@material-ui/core/Radio";
import Switch from "@material-ui/core/Switch";
import FormControlLabel from "@material-ui/core/FormControlLabel";
import Select from "@material-ui/core/Select";
import MenuItem from "@material-ui/core/MenuItem";
import FormControl from "@material-ui/core/FormControl";
import InputLabel from "@material-ui/core/InputLabel";
import Typography from "@material-ui/core/Typography";
import TextField from "@material-ui/core/TextField";

import RecommenderWidget from "../../widget/src/RecommenderWidget";

const drawerWidth = 480;
const styles = {
  settingsHeader: {
    paddingTop: 32,
    paddingBottom: 16
  },
  drawer: {
    width: drawerWidth
  }
};
const appToken = "sRwk1LSuP2YFcbnY";

const sleep = t => new Promise(resolve => setTimeout(resolve, t || 0));
let host =
  location.host === "localhost:8080"
    ? "http://localhost:3000"
    : "https://bibdata.dk";
host = "https://bibdata.dk";
class RecommendAdmin extends React.Component {
  async newSamplePid() {
    this.saveState({
      samplePid: (await axios.get(`${host}/v1/randomPid?token=${appToken}`))
        .data
    });
  }
  constructor() {
    super();
    let site, token;
    try {
      let hash = location.hash.slice(1);
      if (hash.endsWith("?")) {
        hash = hash.slice(0, hash.length - 1);
      }
      const o = JSON.parse(decodeURI(hash));
      site = o.site;
      token = o.token;
      location.hash = "";
    } catch (e) {
      //
    }
    this.state = {
      site: site || localStorage.getItem("bibspireConfigSite") || location.host,
      token: token || localStorage.getItem("bibspireConfigToken") || ""
    };
    (async () =>
      this.setState({
        libraries: (await axios.get(`${host}/v1/libraries?token=${appToken}`))
          .data
      }))();
    this.newSamplePid();
    this.loadConfig();
  }
  async loadConfig() {
    await sleep();
    this.lastSync = Date.now();
    localStorage.setItem("bibspireConfigSite", this.state.site);
    localStorage.setItem("bibspireConfigToken", this.state.token);
    try {
      const config = (await axios.get(
        `${host}/v1/bib/config?site=${this.state.site}&token=${this.state.token}`
      )).data;
      const client = config.config.client || {};
      client.tingObject = client.tingObject || {};
      this.saveState({
        loggedIn: true,
        agencies: config.agencies,
        longtailedness: config.config.longtailedness,
        filter_holdings_agency: config.config.filter_holdings_agency,
        one_per_creator: config.config.one_per_creator,
        require_cover: config.config.require_cover,
        include_ereolen: config.config.include_ereolen,
        widgetType: client.tingObject.widgetType,
        customLimit: client.tingObject.customLimit,
        customHtml: client.tingObject.customHtml,
        rows: client.tingObject.rows,
        parentSelector: client.tingObject.parentSelector,
        coverWidth: client.tingObject.coverWidth
      });
    } catch (e) {
      this.saveState({ loggedIn: false });
    }
  }
  async saveState(state) {
    this.setState(state);
    await sleep();
    if (this.running) {
      this.scheduled = true;
      return;
    }
    this.running = true;

    try {
      const {
        longtailedness,
        filter_holdings_agency,
        one_per_creator,
        require_cover,
        recommenderEngine,
        include_ereolen,
        customLimit,
        customHtml,
        widgetType,
        rows,
        parentSelector,
        coverWidth
      } = this.state;
      await axios.put(
        `${host}/v1/bib/config?site=${this.state.site}&token=${this.state.token}`,
        {
          longtailedness,
          filter_holdings_agency,
          one_per_creator,
          require_cover,
          recommenderEngine,
          include_ereolen,
          client: {
            tingObject: {
              widgetType,
              customLimit,
              customHtml,
              rows,
              parentSelector,
              coverWidth
            }
          }
        }
      );
      this.updateExample();
    } catch (e) {
      console.log(e);
      this.scheduled = true;
    }

    await sleep(400);
    this.running = false;
    if (this.scheduled) {
      this.scheduled = false;
      this.saveState({});
    }
  }
  async updateExample() {
    await sleep();
    const t0 = Date.now();
    let sampleRecommend = axios.get(
      `${host}/v1/recommend?pid=${this.state.samplePid}&site=${this.state.site}&token=${appToken}`
    );
    sampleRecommend = (await sampleRecommend).data;
    this.recommendTime = Date.now() - t0;
    this.setState({ sampleRecommend });
    const sampleManifest = (await axios.get(
      `https://bibdata.dk/v1/object/${this.state.samplePid}?token=${appToken}`
    )).data;
    this.setState({ sampleManifest });
  }
  render() {
    const { state, props } = this;
    const { classes } = props;
    return (
      <div>
        <Drawer
          classes={{ paper: classes.drawer }}
          variant="permanent"
          open={true}
        >
          <Container>
            <FormGroup>
              <TextField
                label="Site"
                value={this.state.site}
                onChange={async o => {
                  this.setState({ site: o.target.value });
                  this.loadConfig();
                }}
                margin="normal"
              />
              <TextField
                label="Token"
                value={this.state.token}
                onChange={async o => {
                  this.setState({ token: o.target.value });
                  this.loadConfig();
                }}
                margin="normal"
              />
            </FormGroup>
            {this.state.loggedIn ? (
              <FormGroup>
                <Typography variant="h4" className={classes.settingsHeader}>
                  Anbefalinger
                </Typography>
                <FormControl component="fieldset">
                  <FormLabel component="legend">Anbefalingsmotor</FormLabel>
                  <RadioGroup
                    value={this.state.recommenderEngine || ""}
                    onChange={e =>
                      this.saveState({ recommenderEngine: e.target.value })
                    }
                  >
                    <FormControlLabel
                      value=""
                      control={<Radio />}
                      label={
                        <span>
                          BibSpire default{" "}
                          <small>
                            (Kun denne er dækket af performancegaranti /
                            service-level-agreement)
                          </small>
                        </span>
                      }
                    />
                    <FormControlLabel
                      value="webtrekk"
                      control={<Radio />}
                      label="BibSpire WebTrekk"
                    />
                    <FormControlLabel
                      value="dbc"
                      control={<Radio />}
                      label={<span>DBC OpenPlatform recommender</span>}
                    />
                    {/*
          <FormControlLabel
            value="disabled"
            disabled
            control={<Radio />}
            label={<span>Blanding <small>(finder bedste kombination af ovenstående)</small></span>}
          />*/}
                  </RadioGroup>
                </FormControl>
                <div>
                  <Typography>
                    <br />
                    <b>Indstillinger for anbefalinger</b> (påvirker kun BibSpire
                    anbefalingemotorer):
                  </Typography>
                  <Slider
                    style={{ padding: "22px 0px" }}
                    value={this.state.longtailedness * 100}
                    onChange={(e, v) =>
                      this.saveState({ longtailedness: v / 100 })
                    }
                  />
                  <Typography style={{ float: "right" }}>Snævre</Typography>
                  <Typography>Populære</Typography>
                </div>
                <div>
                  <Typography>
                    <br />
                    <b>Efterbehandling af anbefalinger</b> – uafhængigt af
                    anbefalingsmotor:
                  </Typography>
                  <FormControlLabel
                    control={
                      <Switch
                        checked={!!this.state.require_cover}
                        onChange={o =>
                          this.saveState({ require_cover: o.target.checked })
                        }
                      />
                    }
                    label="Kun materiale med forside"
                  />
                  <FormControlLabel
                    control={
                      <Switch
                        checked={!!this.state.one_per_creator}
                        onChange={o =>
                          this.saveState({ one_per_creator: o.target.checked })
                        }
                      />
                    }
                    label="Begræns materiale med samme forfatter"
                  />
                </div>
                <Typography variant="h4" className={classes.settingsHeader}>
                  Eksempel
                </Typography>
                <TextField
                  label="Eksempel-pid"
                  value={state.samplePid}
                  onChange={(e, v) =>
                    this.saveState({ samplePid: e.target.value })
                  }
                />
                <Button onClick={() => this.newSamplePid()}>
                  {" "}
                  Ny tilfældig eksempel-pid{" "}
                </Button>
              </FormGroup>
            ) : (
              <Typography>
                Not logged in. Make sure that site and token is correct.
              </Typography>
            )}
          </Container>
        </Drawer>
        <div className={classes.hello} style={{ marginLeft: drawerWidth }}>
          <Container>
            <Typography>
              <h1>Værktøj til tilpasning af anbefalinger.</h1>
              <p>
                Til venstre kan indstillingerne for et givent site tilpasses, -
                og herunder ses preview af hvordan anbefalingerne bliver. Når
                indstillingerne ændres bliver de automatisk gemt, og slår
                igennem på sitet.
              </p>
            </Typography>
            {state.sampleManifest && !state.sampleManifest.error && (
              <div>
                <img
                  src={state.sampleManifest.image}
                  height={200}
                  style={{ float: "left", marginRight: 8 }}
                />
                <Typography>
                  <b>{state.sampleManifest.name}</b>
                  <br />
                  <em>{state.sampleManifest.creator.join(" & ")}</em>
                </Typography>
              </div>
            )}
            <div style={{ clear: "both" }} />
            {state.sampleRecommend && state.sampleRecommend && (
              <RecommenderWidget
                related={state.sampleRecommend.related}
                config={state.sampleRecommend.config.tingObject}
              />
            )}
            <br /> <br /> <br /> <br /> <br /> <br /> <br />
            Anbefalingerne tog {this.recommendTime}ms at hente. Bemærk at kald
            gennem BibSpire giver ekstra transporttid og efterbehandlingtid for
            ikke-BibSpire anbefalingsmotorer, og at det vil være hurtigere at
            kalde dem direkte.
          </Container>
        </div>
      </div>
    );
  }
}

const StyledRecommendAdmin = withStyles(styles)(RecommendAdmin);

document.addEventListener("DOMContentLoaded", async () => {
  let elem = document.getElementById("recommend-admin");
  if (!elem) {
    elem = document.createElement("div");
    elem.id = "recommend-admin";
    document.body.appendChild(elem);
  }
  ReactDOM.render(<StyledRecommendAdmin />, elem);
});
